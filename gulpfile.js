var gulp = require('gulp'),
    livereload = require('gulp-livereload'),
    nunjucksRender = require('gulp-nunjucks-render'),
    data = require('gulp-data'),
    sass = require('gulp-sass'),
    imagemin = require('gulp-imagemin'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCss = require('gulp-clean-css'),
    concatCss = require('gulp-concat-css'),
    concat = require('gulp-concat'),
    htmlmin = require('gulp-htmlmin'),
    uglify = require('gulp-uglify'),
    awsPublish = require('gulp-awspublish');

var dest = './dist';

gulp.task('nunjucks', function() {
    return gulp.src('./src/*.njk')
        .pipe(data(function() {
            return require('./src/data.json')
        }))
        .pipe(nunjucksRender({
            path: ['./src']
        }))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest(dest))
        .pipe(livereload());
});

gulp.task('sass', function () {
    var files = ['reset', 'common', 'layout', 'titles', 'tour-list', 'faq',
        'tour-city-overlook', 'top-menu-fade', 'slicknav_custom'];
    files = files.map(function (f) {
            return './src/styles/' + f + '.scss';
    });
    console.log(files);
    return gulp.src(files)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCss())
        .pipe(concatCss("styles/bundle.css"))
        .pipe(gulp.dest(dest))
        .pipe(livereload());
});

gulp.task('slicknav-css', function () {
    return gulp.src(['./node_modules/slicknav/dist/slicknav.css'])
        .pipe(gulp.dest('./dist/styles'));
});

gulp.task('slicknav', ['slicknav-css'], function () {
    return gulp.src(['./node_modules/slicknav/dist/jquery.slicknav.js'])
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('pikaday-css', function () {
    return gulp.src(['./node_modules/pikaday/css/pikaday.css'])
        .pipe(gulp.dest('./dist/styles'));
});

gulp.task('pikaday', ['pikaday-css'], function () {
    return gulp.src(['./node_modules/pikaday/pikaday.js',
        './node_modules/pikaday/plugins/pikaday.jquery.js'])
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('js', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(concat('bundle.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
        .pipe(livereload());
});

gulp.task('img', function () {
    return gulp.src('./src/img/**/*.*')
        //.pipe(imagemin())
        .pipe(gulp.dest('./dist/img'))
        .pipe(livereload());
});

gulp.task('publish', function () {
    var publisher = awsPublish.create({
        params: {
            Bucket: 'aws-website-latours-yckiu'
        }
    });

    return gulp.src("./dist/**")
        .pipe(publisher.publish())
        .pipe(publisher.sync())
        .pipe(publisher.cache())
        .pipe(awsPublish.reporter());
});

gulp.task('build', ['nunjucks', 'sass', 'js', 'img', 'pikaday', 'slicknav'], function () {
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('./src/**/*', ['build']);
});
