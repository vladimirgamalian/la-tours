
$(document).ready(function () {

    $('#topmenu').slicknav({
        appendTo: '.top-nav-menu',
        label: '',
        duplicate: false,
        closeOnClick: true
    });


    $('.scroll').on('click', function(event){
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
        //$('.top-nav-menu').removeClass('top-nav-menu-visible').addClass('top-nav-menu-invisible');
        return false;
    });

});
