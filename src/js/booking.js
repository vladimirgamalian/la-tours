
var currentBookStep = 1;
var bookingAdditions = [false, false, false];

$(document).ready(function () {
    var $datepicker = $('#datepicker').pikaday({
        firstDay: 0,
        minDate: new Date(2000, 0, 1),
        maxDate: new Date(2020, 12, 31),
        yearRange: [2000, 2020]
    });

    //updateBookStepVisibility();
});


// Настраивает заголовоки шагов букинга (делает последний незавершенным, а предыдущие завершенным).
function setBookStepHeader(step) {
    var id, s, idIcon;
    for (s = 1; s <= 4; s += 1) {
        id = "#book-step-header-" + s;
        idIcon = id + " > i";
        if (s <= step) {
            $(id).removeClass("display-none book-step-ready");
            if (s < step) {
                $(id).addClass("book-step-ready");
                $(idIcon).removeClass("fa-chevron-down");
                $(idIcon).addClass("fa-check-circle-o");
            } else {
                $(idIcon).removeClass("fa-check-circle-o");
                $(idIcon).addClass("fa-chevron-down");
            }
        } else {
            $(id).addClass("display-none");
        }
    }
}

// Включает нужный шаг букинга.
function setBookStep(step) {
    var id, s;
    for (s = 1; s <= 4; s += 1) {
        id = "#book-step-" + s;
        if (s == step) {
            $(id).removeClass("display-none");
        } else {
            $(id).addClass("display-none");
        }
    }
}

function updateBookStepVisibility() {
    setBookStepHeader(currentBookStep);
    setBookStep(currentBookStep);
}

function switchBookStep(step) {
    if (currentBookStep != step) {
        currentBookStep = step;
        updateBookStepVisibility();
    }
}

function getTotalPrice() {
    var prices = [29, 59, 79],
        result  = 0,
        i;

    for (i = 0; i < 3; i += 1) {
        if (bookingAdditions[i]) {
            result += prices[i];
        }
    }

    return result;
}

function updateTotalPrice() {
    var totalPrice = getTotalPrice();
    $(".total-price").text("$" + totalPrice);
}

function updateBookingAdditions() {
    var i, id;
    for (i = 1; i <= 3; i += 1) {
        id = "#book-step-3-select-" + i;
        if (bookingAdditions[i - 1]) {
            $(id).removeClass("additions-icon");
            $(id).addClass("additions-icon-selected");
        } else {
            $(id).removeClass("additions-icon-selected");
            $(id).addClass("additions-icon");
        }
    }

    updateTotalPrice();
}

$(document).ready(function () {
    var i, id;

    $( "#book-step-1-select-1" ).click(function(event) {
        event.preventDefault();
        switchBookStep(2);
        app.mySwiper1b.update();
    });
    $( "#book-step-1-select-2" ).click(function(event) {
        event.preventDefault();
        switchBookStep(2);
        app.mySwiper1b.update();
    });

    $( "#book-step-2-select-1" ).click(function(event) {
        event.preventDefault();
        switchBookStep(3);
        app.mySwiper1b.update ();
    });
    $( "#book-step-2-select-2" ).click(function(event) {
        event.preventDefault();
        switchBookStep(3);
        setBookStep(3);
    });


    // Клики по заголовкам шагов сразу переводят на нужный шаг.
    $( "#book-step-header-1" ).click(function(event) {
        event.preventDefault();
        switchBookStep(1);
    });
    $( "#book-step-header-2" ).click(function(event) {
        event.preventDefault();
        switchBookStep(2);
    });
    $( "#book-step-header-3" ).click(function(event) {
        event.preventDefault();
        switchBookStep(3);
    });
    $( "#book-step-header-4" ).click(function(event) {
        event.preventDefault();
        switchBookStep(4);
    });


    // Дополнения.
    $( "#book-step-3-select-1" ).click(function(event) {
        event.preventDefault();
        bookingAdditions[0] = !bookingAdditions[0];
        updateBookingAdditions();
    });
    $( "#book-step-3-select-2" ).click(function(event) {
        event.preventDefault();
        bookingAdditions[1] = !bookingAdditions[1];
        updateBookingAdditions();
    });
    $( "#book-step-3-select-3" ).click(function(event) {
        event.preventDefault();
        bookingAdditions[2] = !bookingAdditions[2];
        updateBookingAdditions();
    });

    updateBookingAdditions();
});
