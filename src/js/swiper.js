

var app = {};

$(document).ready(function () {
    app.mySwiper1 = new Swiper ('.swiper-container1', {
        pagination: '.swiper-pagination1',
        loop: true
    });
    app.mySwiper1b = new Swiper ('.swiper-container1b', {
        pagination: '.swiper-pagination1b',
        loop: true
    });
    app.mySwiper2 = new Swiper ('.swiper-container2', {
        pagination: '.swiper-pagination2',
        loop: true
    });
    app.mySwiper3 = new Swiper ('.swiper-container3', {
        pagination: '.swiper-pagination3',
        loop: true
    });

    app.swiperTourList = new Swiper ('.swiper-container-tour-list', {
        pagination: '.swiper-pagination-tour-list',
        loop: true
    });

    app.swiperTourList = new Swiper ('.swiper-container-tour-list-city-overlook', {
        pagination: '.swiper-pagination-tour-list-city-overlook',
        loop: true
    });

    
});
