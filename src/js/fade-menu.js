
$(document).ready(function () {
    var lastScrollTop = 0;
    $(window).scroll(function(event){
        var st = $(this).scrollTop();
        if (st > lastScrollTop){
            $('.top-nav-menu').removeClass('top-nav-menu-visible').addClass('top-nav-menu-invisible');
        } else {
            $('.top-nav-menu').removeClass('top-nav-menu-invisible').addClass('top-nav-menu-visible');
        }
        lastScrollTop = st;
    });
});
